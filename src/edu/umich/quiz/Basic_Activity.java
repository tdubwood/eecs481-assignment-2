package edu.umich.quiz;


import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Basic_Activity extends Activity {
	
	private Button mYesButton;
	private Button mNoButton;
	private TextView mTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_basic_);
		mTextView = (TextView) findViewById(R.id.text_view);
		
		mYesButton = (Button) findViewById(R.id.yes_button);
		mYesButton.setOnClickListener(new View.OnClickListener() {
			//@Override
			public void onClick(View v) {
				//toast good message
				Toast.makeText(Basic_Activity.this, R.string.yes_toast, Toast.LENGTH_LONG).show();
				//change button text
				mYesButton.setTextColor(Color.CYAN);
				mNoButton.setTextColor(Color.BLACK);
				// Change text to 'You passed Quiz!'
				mTextView.setText("You have passed the 481 quiz. Congrats.");
				
			}
			
			
		});
		mNoButton = (Button) findViewById(R.id.no_button);
		mNoButton.setOnClickListener(new View.OnClickListener() {
			//@Override
			public void onClick(View v) {
				// Toast bad message
				Toast.makeText(Basic_Activity.this, R.string.no_toast, Toast.LENGTH_LONG).show();
				//change button text
				mNoButton.setTextColor(Color.CYAN);
				mYesButton.setTextColor(Color.BLACK);
				// Change text to boo
				mTextView.setText("You have failed the 481 quiz. How sad.");				
				
			}
			
			
		}); 
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.basic_, menu);
		return true;
	}

}
